import pandas as pd
import numpy as np 
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.model_selection import train_test_split
from tensorflow.python.keras.losses import BinaryCrossentropy
from tensorflow.python.keras.metrics import BinaryAccuracy
import tensorflow as tf
from tensorflow.keras.layers import Dense, Dropout
from tensorflow import keras
from sklearn.metrics import confusion_matrix, f1_score, precision_recall_curve, PrecisionRecallDisplay, plot_precision_recall_curve
import matplotlib.pyplot as plt



#DATA_PATH= 'D:/data/kaggle/home_credit/application_processed.csv'
DATA_PATH= '/home/lauh/projects/def-gambsseb/lauh/data/application_processed.csv'

DROP_COL= []
OHE_COL= []
SCALE_COL= []
Y_COL= 'TARGET'
OHE_PATTERNS= ['NAME','OCCUPATION_TYPE', 'STATUS']
FLAG_PATTERNS= ['FLAG']

def get_cat_col(df, patterns= OHE_PATTERNS):
    cat_col=[]
    for pattern in patterns:
        for col in df.columns:
            if pattern in col:
                cat_col.append(col)
    return list(set(cat_col))

def get_flag_col(df, patterns= FLAG_PATTERNS):
    flag_col=[]
    for pattern in patterns:
        for col in df.columns:
            if pattern in col:
                flag_col.append(col)
    return list(set(flag_col))

def load_data(data_path=DATA_PATH, drop_col= DROP_COL):
    df= pd.read_csv(data_path)
    df= df.drop(columns= drop_col)
    return df

def split_train_test(df, y= Y_COL):
    df_y= df[y]
    df_x= df.drop(columns= y)
    train_x, test_x, train_y, test_y= train_test_split(df_x, df_y, test_size=0.2, stratify=df_y, random_state=42)
    return train_x, test_x, train_y, test_y

def scale_col(train_x, test_x, sc_col= SCALE_COL):
    sc= StandardScaler()
    train_sc= train_x.drop(columns=[col for col in train_x.columns if col not in sc_col])
    test_sc= test_x.drop(columns= [col for col in test_x.columns if col not in sc_col])
    train_sc[sc_col]= sc.fit_transform(train_x[sc_col])
    test_sc[sc_col]= sc.transform(test_x[sc_col])
    return train_sc, test_sc

def ohe(train_x, test_x, ohe_col=OHE_COL):
    ohe= OneHotEncoder(sparse=False)
    train_ohe= ohe.fit_transform(train_x[ohe_col])
    test_ohe= ohe.transform(test_x[ohe_col])
    return pd.DataFrame(train_ohe, columns= ohe.get_feature_names()), pd.DataFrame(test_ohe, columns= ohe.get_feature_names())

def load_preprocess():
    df= load_data()
    train_x, test_x, train_y, test_y= split_train_test(df)
    ohe_col= get_cat_col(df)
    flag_col= get_flag_col(df)
    sc_col= [col for col in df.columns if col not in ohe_col+flag_col+['TARGET']]
    train_sc, test_sc= scale_col(train_x, test_x, sc_col)
    train_ohe, test_ohe= ohe(train_x, test_x, ohe_col)
    train_x= pd.concat([train_sc.reset_index(drop=True), train_ohe.reset_index(drop=True), train_x[flag_col].reset_index(drop=True)], axis=1)
    test_x= pd.concat([test_sc.reset_index(drop=True), test_ohe.reset_index(drop=True), test_x[flag_col].reset_index(drop=True)], axis=1)
    return train_x, test_x, train_y.reset_index(drop=True), test_y.reset_index(drop=True)

def build_model():
    inputs = keras.Input(shape=(365,), name="inputs")
    drop= Dropout(0.3)(inputs)
    x = Dense(150, activation="relu", name="dense_1")(drop)
    drop= Dropout(0.5)(x)
    x = Dense(70, activation="relu", name="dense_2")(drop)
    drop= Dropout(0.5)(x)
    outputs = Dense(1, activation="sigmoid", name="predictions")(drop)
    model = keras.Model(inputs=inputs, outputs=outputs)
    model.compile(optimizer=keras.optimizers.Adam(learning_rate=.00005)
                    , loss= keras.losses.BinaryCrossentropy()
                    , metrics= [keras.metrics.BinaryAccuracy()])
    return model

def train_model(model, train_x, test_x, train_y, test_y):
    total=train_y.shape[0]
    pos= train_y.sum()
    neg= total - pos
    weight_for_0 = (1 / neg) * (total / 2.0)
    weight_for_1 = (1 / pos) * (total / 2.0)
    class_weight = {0: weight_for_0, 1: weight_for_1}
    #neg, pos = np.bincount(train_y)
    #class_weight = {0: train_y.shape[0]/neg, 1:  train_y.shape[0]/pos}
    history = model.fit(
        train_x,
        train_y,
        batch_size=64,
        epochs=200,
        validation_data=(test_x, test_y),
        class_weight= class_weight
    )
    return history

def evaluate_model(model, history, test_x, test_y):
    plt.plot(history.history['loss'], label='loss')
    plt.plot(history.history['val_loss'], label='val_loss')
    pred= model.predict(test_x)
    print('confusion matrix', confusion_matrix(test_y, np.where(pred>0.5,1,0)))
    print('f1 score', f1_score(test_y,  np.where(pred>0.5,1,0)))


def create_adversarial_pattern(model, input_client, input_label, loss_object):
    with tf.GradientTape() as tape:
        tape.watch(input_client)
        prediction = model(input_client)
        loss = loss_object(input_label, prediction)
    # Get the gradients of the loss w.r.t to the input image.
    gradient = tape.gradient(loss, input_client)
    # Get the sign of the gradients to create the perturbation
    signed_grad = tf.sign(gradient)
    return signed_grad

train_x, test_x, train_y, test_y= load_preprocess()
train_x, train_x2= train_x.iloc[:int(train_x.shape[0]*.5),:], train_x.iloc[int(train_x.shape[0]*.5):,:]
train_y, train_y2= train_y.iloc[:int(train_y.shape[0]*.5)], train_y.iloc[int(train_y.shape[0]*.5):]
model= build_model()
history= train_model(model, train_x, test_x, train_y, test_y)
print('model 1')
evaluate_model(model, history, test_x, test_y)

loss_object = keras.losses.BinaryCrossentropy()

#whitebox attack


def fast_gradient_attack(samples_x, samples_y, model, loss_object, eps=.1):
    tensor_x, tensor_y= tf.convert_to_tensor(samples_x.to_numpy().reshape((samples_x.shape[0],samples_x.shape[1]))), tf.convert_to_tensor(samples_y.to_numpy().reshape((samples_y.shape[0],1)))
    adv_noise= create_adversarial_pattern(model, tensor_x, tensor_y, loss_object)
    adv_exemple= tensor_x+ eps * adv_noise
    return adv_exemple

model2= build_model()
history= train_model(model2, train_x2, test_x, train_y2, test_y)
print('model 2')
evaluate_model(model2, history, test_x, test_y)

pred= model2.predict(test_x)
test_pred= pd.DataFrame({'TARGET':test_y, 'pred':np.where(pred>=.5,1,0).reshape(-1,)})
tp_x, tp_y= test_x.loc[(test_pred.TARGET==1)&(test_pred.pred==1),:], test_y.loc[(test_pred.TARGET==1)&(test_pred.pred==1)]

print('number of TP:', tp_x.shape[0])
eps, change_pct=[],[]
for i in np.arange(0.001,1.001,.001):
    adv = fast_gradient_attack(tp_x, tp_y, model, loss_object, i)
    adv_pred= np.where(model2.predict(adv)>=.5,1,0)
    #print('eps',i)
    nb_change=(tp_x.shape[0]-adv_pred.sum())
    #print('number of adverserial examples that change prediction', nb_change, 'or', nb_change/tp_x.shape[0])
    eps.append(i)
    change_pct.append(nb_change/tp_x.shape[0])
pd.DataFrame({'eps': eps, 'pct_change':change_pct}).to_csv('adverserial_eps2.csv', index=False)





"""
original_pred= model.predict(tf.convert_to_tensor(test_x.iloc[1,:].to_numpy().reshape((1,train_x.shape[1]))))
adv_noise= create_adversarial_pattern(model, tf.convert_to_tensor(test_x.iloc[1,:].to_numpy().reshape((1,train_x.shape[1]))), tf.convert_to_tensor(test_y[1].reshape((1,1))), loss_object)
eps=.5
adv_exemple= tf.convert_to_tensor(test_x.iloc[1,:].to_numpy().reshape((1,train_x.shape[1])))+ eps * adv_noise
adv_pred= model.predict(adv_exemple)
print('original pred: {}, \n adv pred {}'.format(original_pred, adv_pred))

original_pred= model.predict(tf.convert_to_tensor(test_x.iloc[2,:].to_numpy().reshape((1,11))))
adv_noise= create_adversarial_pattern(model, tf.convert_to_tensor(test_x.iloc[2,:].to_numpy().reshape((1,11))), tf.convert_to_tensor(test_y[2].reshape((1,1))), loss_object)
eps=.1
adv_exemple= tf.convert_to_tensor(test_x.iloc[2,:].to_numpy().reshape((1,11)))+ eps * adv_noise
adv_pred= model.predict(adv_exemple)

print('original pred: {}, \n adv pred {}'.format(original_pred, adv_pred))
"""